﻿using CommandLine;
using CommandLine.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AwsConsoleUploader
{
    class Options
    {
        [Option('i', "input-file", Required = true,
    HelpText = "Input file to be processed.  Can specify just the filename or specify fullpath to file.")]
        public string InputFilepath { get; set; }

        [Option('o', "output-file-s3", Required= true,
HelpText = "Output file saved to AWS.  Can specify the filename or specify the cloud path.")]
        public string AWSFilename { get; set; }

        [Option('d', "debug", DefaultValue = false,
          HelpText = "Prints all messages to standard output.")]
        public bool DebugFlag { get; set; }

        [Option('c', "content-type",
          HelpText = "Sets the content type for AWS.")]
        public String ContentType { get; set; }

        [Option('s', "success-log-filepath",
          HelpText = "Saves success log to specified file location.")]
        public String SuccessLogFilepath { get; set; }

        [Option('f', "error-log-filepath",
          HelpText = "Saves error log to specified file location.")]
        public String ErrorLogFilepath { get; set; }

        [Option('m', "meta-data-custom",
  HelpText = "Format is 'key:value'.  Specify a custom metadata key value pair to add to the uploaded file. ")]
        public String MetadataKVP { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}
