﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.Runtime;

namespace AwsConsoleUploader
{
    class Program
    {
        public static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder(1024);
            Options options = new Options();
            using (StringWriter sr = new StringWriter(sb))
            {
                try
                {
                    //Notes:  The recommended approach for storing credentials is in the SDK Store. To register a new profile, use the following line of code.  Make sure to set the profile name in the app.config as well.  The credentials are stored in the active user's home directory.
                    //Amazon.Util.ProfileManager.RegisterProfile("testProfileName", "testAccessKey", "testSecretKey");
                    IAmazonS3 s3Client = AWSClientFactory.CreateAmazonS3Client();

                    //Notes: TransferUtility was used in order to abstract out multi-part uploads that would have been required for larger upload sizes.                
                    TransferUtilityUploadRequest ul = new TransferUtilityUploadRequest();

                    //Test inputs for development.
                    //var path = "-i C:\\Reqs.txt -o path/outfile.txt -c .text/plain -d -f C:\\Users\\Public -s C:\\Users\\Public -m key:pair";
                    //var inputParams = path.Split(' ');

                    //Try parsing arguments from input.
                    if (CommandLine.Parser.Default.ParseArguments(args, options))
                    {
                        // Values are available here
                        sr.WriteLine("InputFilename: {0}", options.InputFilepath);
                        sr.WriteLine("AWSFilename: {0}", options.AWSFilename);
                        sr.WriteLine("ShowDebugFlag: {0}", options.DebugFlag);
                        sr.WriteLine("ErrorLogFilepath: {0}", options.ErrorLogFilepath);
                        sr.WriteLine("SuccessLogFilepath: {0}", options.SuccessLogFilepath);
                        sr.WriteLine("Content-Type: {0}", options.ContentType);
                        sr.WriteLine("Custom MetaData KVP: {0}", options.MetadataKVP);

                        //Setting Parameters for AWS.

                        //Bucketname - Stored in the app config for easy access.
                        ul.BucketName = ConfigurationManager.AppSettings["BucketName"];
                        

                        //Set Where AWS should upload file from.                        
                        ul.FilePath = options.InputFilepath;

                        //Set AWS Filename - Takes input filename if no specific name is specified.
                        ul.Key = options.AWSFilename;

                        //Set Metadata KVP
                        if (!String.IsNullOrEmpty(options.MetadataKVP))
                        {
                            var kvp = options.MetadataKVP.Split(':');
                            ul.Metadata.Add(kvp[0], kvp[1]);
                        }

                        //Set Content Type
                        if (!String.IsNullOrEmpty(options.ContentType))
                        {
                            ul.ContentType = options.ContentType;
                        }
                    }

                    TransferUtility util = new TransferUtility(s3Client);
                    util.Upload(ul);
                    sr.WriteLine("Upload Successful!");

                    if(!String.IsNullOrEmpty(options.SuccessLogFilepath))
                        System.IO.File.WriteAllText(options.SuccessLogFilepath + "\\SuccessLog.txt", sb.ToString());
                    //Call Success Log Here... or if I change upload to asynchronous.. use a delegate method or a observer.
                }

                //Catch AmazonS3 Related Exceptions.
                catch (AmazonS3Exception ex)
                {
                    if (ex.ErrorCode != null && (ex.ErrorCode.Equals("InvalidAccessKeyId") ||
                        ex.ErrorCode.Equals("InvalidSecurity")))
                    {
                        sr.WriteLine("Please check the provided AWS Credentials.");
                    }
                    else
                    {
                        sr.WriteLine("Caught Exception: " + ex.Message);
                        sr.WriteLine("Response Status Code: " + ex.StatusCode);
                        sr.WriteLine("Error Code: " + ex.ErrorCode);
                        sr.WriteLine("Request ID: " + ex.RequestId);
                    }

                    if (!String.IsNullOrEmpty(options.ErrorLogFilepath))
                        System.IO.File.WriteAllText(options.ErrorLogFilepath + "\\ErrorLog.txt", sb.ToString());
                }
                    //Catch all generic exceptions
                catch (Exception ex)
                {
                    sr.WriteLine("\nError: " + ex.Message);

                    if (!String.IsNullOrEmpty(options.ErrorLogFilepath))
                        System.IO.File.WriteAllText(options.ErrorLogFilepath + "\\ErrorLog.txt", sb.ToString());
                }

            }

            if(options.DebugFlag)
                Console.Write(sb);
        }
    }
}